# Sourcing example
``` terraform
module "logging" {
  source                                 = "git::https://gitlab.com/Stargeras/module-plg"
  ingress_domain                         = "example.com"
  kubernetes_auth_host                   = module.kubernetes.kubernetes_auth_host
  kubernetes_auth_cluster_ca_certificate = module.kubernetes.kubernetes_auth_cluster_ca_certificate
  kubernetes_auth_client_certificate     = module.kubernetes.kubernetes_auth_client_certificate
  kubernetes_auth_client_key             = module.kubernetes.kubernetes_auth_client_key
  kubernetes_auth_token                  = module.kubernetes.kubernetes_auth_token
}
```
<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_plg"></a> [plg](#module\_plg) | ./modules/plg | n/a |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_enable_grafana"></a> [enable\_grafana](#input\_enable\_grafana) | Enable Grafana deployment. Defaults to false because Grafana is installed in https://gitlab.com/Stargeras/module-monitoring-stack. | `bool` | `false` | no |
| <a name="input_grafana_admin_password"></a> [grafana\_admin\_password](#input\_grafana\_admin\_password) | Initial password for Grafana. If left blank, will be randomly generated. | `string` | `""` | no |
| <a name="input_ingress_domain"></a> [ingress\_domain](#input\_ingress\_domain) | Domain name for ingress endpoints | `string` | `"example.com"` | no |
| <a name="input_kubernetes_auth_client_certificate"></a> [kubernetes\_auth\_client\_certificate](#input\_kubernetes\_auth\_client\_certificate) | PEM-encoded client certificate for TLS authentication | `string` | `""` | no |
| <a name="input_kubernetes_auth_client_key"></a> [kubernetes\_auth\_client\_key](#input\_kubernetes\_auth\_client\_key) | PEM-encoded client certificate key for TLS authentication | `string` | `""` | no |
| <a name="input_kubernetes_auth_cluster_ca_certificate"></a> [kubernetes\_auth\_cluster\_ca\_certificate](#input\_kubernetes\_auth\_cluster\_ca\_certificate) | PEM-encoded root certificates bundle for TLS authentication | `string` | n/a | yes |
| <a name="input_kubernetes_auth_host"></a> [kubernetes\_auth\_host](#input\_kubernetes\_auth\_host) | The hostname (in form of URI) of the Kubernetes API | `string` | n/a | yes |
| <a name="input_kubernetes_auth_token"></a> [kubernetes\_auth\_token](#input\_kubernetes\_auth\_token) | Service account token | `string` | `""` | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->