variable "kubernetes_auth_host" {
  description = "The hostname (in form of URI) of the Kubernetes API"
  type        = string
}
variable "kubernetes_auth_cluster_ca_certificate" {
  description = "PEM-encoded root certificates bundle for TLS authentication"
  type        = string
}
variable "kubernetes_auth_client_certificate" {
  default     = ""
  description = "PEM-encoded client certificate for TLS authentication"
  type        = string
}
variable "kubernetes_auth_client_key" {
  default     = ""
  description = "PEM-encoded client certificate key for TLS authentication"
  type        = string
}
variable "kubernetes_auth_token" {
  default     = ""
  description = "Service account token"
  type        = string
}

variable "ingress_domain" {
  default     = "example.com"
  description = "Domain name for ingress endpoints"
  type        = string
}

variable "enable_grafana" {
  default     = false
  description = "Enable Grafana deployment. Defaults to false because Grafana is installed in https://gitlab.com/Stargeras/module-monitoring-stack."
  type        = bool
}

variable "grafana_admin_password" {
  default     = ""
  description = "Initial password for Grafana. If left blank, will be randomly generated."
  type        = string
}