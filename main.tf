module "plg" {
  source                 = "./modules/plg"
  ingress_domain         = var.ingress_domain
  enable_grafana         = var.enable_grafana
  grafana_admin_password = var.grafana_admin_password
}