resource "helm_release" "loki-stack" {
  name             = "loki-stack"
  namespace        = "logging"
  create_namespace = "true"
  repository       = "https://grafana.github.io/helm-charts"
  chart            = "loki-stack"

  values = [
    templatefile("${path.module}/values-tmpl.yaml", {
      ingress_domain         = var.ingress_domain
      enable_grafana         = var.enable_grafana
      grafana_admin_password = var.grafana_admin_password
    })
  ]
}
